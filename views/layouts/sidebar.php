<div id="wrapper" class="col-md-12">
    <div id="sidebar-wrapper" class="col-md-3">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#0" style="cursor: default; font-size: 16px;">
                    <b>Mini Car Inventory System</b>
                </a>
            </li>
            <li>
                <a href="dashboard">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="add-manufacturer">
                    Add Manufacturer
                </a>
            </li>
            <li>
                <a href="add-model">
                    Add Model
                </a>
            </li>
        </ul>
    </div>

    <div id="sidebar-mobile">
        <ul class="sidebar-mobile-nav">
            <li class="">
                <a href="#0" style="cursor: default; font-size: 16px;">
                    <b>Mini Car Inventory System</b>
                </a>
            </li>
            <li>
                <a href="dashboard">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="add-manufacturer">
                    Add Manufacturer
                </a>
            </li>
            <li>
                <a href="add-model">
                    Add Model
                </a>
            </li>
        </ul>
    </div>

